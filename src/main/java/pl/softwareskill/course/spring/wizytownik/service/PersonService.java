package pl.softwareskill.course.spring.wizytownik.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.softwareskill.course.spring.wizytownik.dto.Person;
import pl.softwareskill.course.spring.wizytownik.repository.PersonRepository;

@Service
@AllArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;

    public Person getPerson() {
        return personRepository.getPerson();
    }
}
