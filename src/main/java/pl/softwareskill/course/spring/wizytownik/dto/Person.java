package pl.softwareskill.course.spring.wizytownik.dto;

import lombok.*;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public final class Person {

    Long id;
    UUID personUuid;
    String name;
    String surname;
    String email;
    String telephone;
    Address address;
}
