package pl.softwareskill.course.spring.wizytownik.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.softwareskill.course.spring.wizytownik.service.PersonService;

@Controller
@RequestMapping("person")
@AllArgsConstructor
public class BusinessCardController {

    private final PersonService personService;

    @GetMapping("details")
    public String getPerson(Model model) {
        model.addAttribute("person", personService.getPerson());

        return "details";
    }
}
