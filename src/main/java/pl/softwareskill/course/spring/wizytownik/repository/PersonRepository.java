package pl.softwareskill.course.spring.wizytownik.repository;

import org.springframework.stereotype.Repository;
import pl.softwareskill.course.spring.wizytownik.dto.Address;
import pl.softwareskill.course.spring.wizytownik.dto.Person;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class PersonRepository {

    private Map<UUID, Person> persons = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        final UUID randomUuid = UUID.randomUUID();
        persons.put(randomUuid, buildPerson(randomUuid));
    }

    public Person getPerson() {
        final Set<UUID> keys = persons.keySet();
        return persons.get(keys.toArray()[new Random().nextInt(keys.size())]);
    }

    private Person buildPerson(final UUID personUuid) {
        return Person.builder()
                .id(1L)
                .personUuid(personUuid)
                .name("Andrzej")
                .surname("Kwiatkowski")
                .email("andrzej.kowalski@gmail.com")
                .telephone("504444499")
                .address(getAddress())
                .build();
    }

    private Address getAddress() {
        return Address.builder()
                .id(1L)
                .addressUuid(UUID.randomUUID())
                .city("Katowice")
                .street("Korfantego")
                .streetNumber(1)
                .homeNumber(2)
                .build();
    }
}
